
document.getElementById("enterPswIcon").addEventListener("click", (ev) => {

	showInputValue("enterPassword");
	toggleEyeIcon("enterPswIcon");
});

document.getElementById("confirmPswIcon").addEventListener("click", (ev) => {

	showInputValue("confirmPassword");
	toggleEyeIcon("confirmPswIcon");
});

document.querySelector(".btn").addEventListener("mousedown", (event) => {

	event.preventDefault();

	if (validatePassword()) {
		hideError()
		setTimeout(() => alert('You are welcome'), 10); // setTimeout - для выполнения hideError() преред alert('You are welcome'), а не после
	} else {
		showError();
	};
});


function showInputValue(value) {

	const inputType = document.getElementById(value).getAttribute("type");

	if (inputType == "password") {
		document.getElementById(value).setAttribute("type", "text");
	} else {
		document.getElementById(value).setAttribute("type", "password");
	}
}

function toggleEyeIcon(value) {

	const eyeIconClassList = document.getElementById(value).classList;

	if (eyeIconClassList.contains("fa-eye")) {
		eyeIconClassList.replace("fa-eye", "fa-eye-slash");
	} else {
		eyeIconClassList.replace("fa-eye-slash", "fa-eye");
	}
}

function validatePassword() {

	const enterinputValue = document.getElementById("enterPassword").value;
	const confirminputValue = document.getElementById("confirmPassword").value;

	if (enterinputValue === confirminputValue && enterinputValue !== "" && confirminputValue !== "") {
		return true;
	} return false;

}

function showError() {
	const errorElement = document.createElement('p');
	errorElement.classList.add("error-item")
	errorElement.innerText = "Нужно ввести одинаковые значения";
	document.querySelector(".btn").before(errorElement);

}
function hideError() {
	if (document.querySelector(".error-item") !== null) { // удаляем ошибку при введении одинаковых значений, когда еще не существует error 'p'
		document.querySelector(".error-item").remove();
	}

}